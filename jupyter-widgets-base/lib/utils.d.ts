import { JSONObject } from '@lumino/coreutils';
/**
 * Find all strings in the first argument that are not in the second.
 */
export declare function difference(a: string[], b: string[]): string[];
/**
 * Compare two objects deeply to see if they are equal.
 */
export declare function isEqual(a: unknown, b: unknown): boolean;
/**
 * A polyfill for Object.assign
 *
 * This is from code that Typescript 2.4 generates for a polyfill.
 */
export declare const assign: any;
/**
 * Generate a UUID
 *
 * http://www.ietf.org/rfc/rfc4122.txt
 */
export declare function uuid(): string;
/**
 * A simple dictionary type.
 */
export type Dict<T> = {
    [keys: string]: T;
};
/**
 * Resolve a promiseful dictionary.
 * Returns a single Promise.
 */
export declare function resolvePromisesDict<V>(d: Dict<PromiseLike<V> | V>): Promise<Dict<V>>;
/**
 * Creates a wrappable Promise rejection function.
 *
 * Creates a function that logs an error message before rethrowing
 * the original error that caused the promise to reject.
 */
export declare function reject(message: string, log: boolean): (error: Error) => never;
/**
 * Takes an object 'state' and fills in buffer[i] at 'path' buffer_paths[i]
 * where buffer_paths[i] is a list indicating where in the object buffer[i] should
 * be placed
 * Example: state = {a: 1, b: {}, c: [0, null]}
 * buffers = [array1, array2]
 * buffer_paths = [['b', 'data'], ['c', 1]]
 * Will lead to {a: 1, b: {data: array1}, c: [0, array2]}
 */
export declare function put_buffers(state: Dict<BufferJSON>, buffer_paths: (string | number)[][], buffers: (DataView | ArrayBuffer | ArrayBufferView | {
    buffer: ArrayBuffer;
})[]): void;
export interface ISerializedState {
    state: JSONObject;
    buffers: ArrayBuffer[];
    buffer_paths: (string | number)[][];
}
export interface ISerializeable {
    toJSON(options?: {}): JSONObject;
}
export type BufferJSON = {
    [property: string]: BufferJSON;
} | BufferJSON[] | string | number | boolean | null | ArrayBuffer | DataView;
export declare function isSerializable(object: unknown): object is ISerializeable;
export declare function isObject(data: BufferJSON): data is Dict<BufferJSON>;
/**
 * The inverse of put_buffers, return an objects with the new state where all buffers(ArrayBuffer)
 * are removed. If a buffer is a member of an object, that object is cloned, and the key removed. If a buffer
 * is an element of an array, that array is cloned, and the element is set to null.
 * See put_buffers for the meaning of buffer_paths
 * Returns an object with the new state (.state) an array with paths to the buffers (.buffer_paths),
 * and the buffers associated to those paths (.buffers).
 */
export declare function remove_buffers(state: BufferJSON | ISerializeable): ISerializedState;
export declare const BROKEN_FILE_SVG_ICON = "<svg style=\"height:50%;max-height: 50px;\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 48 48\">\n<g >\n  <g transform=\"translate(0.24520123,0.93464292)\">\n    <path  d=\"M 8.2494641,21.074514 V 5.6225142 c 0,-0.314 0.254,-0.567 0.57,-0.567 H 29.978464 c 2.388,0 9.268,5.8269998 9.268,8.3029998 v 5.5835 l -3.585749,4.407396 -2.772971,-3.535534 -5.126524,3.414213 -5.944543,-3.237436 -5.722718,3.06066 z m 30.9969999,3.8675 v 15.5835 c 0,0.314 -0.254,0.567 -0.57,0.567 H 8.8194641 c -0.315,0.002 -0.57,-0.251 -0.57,-0.566 v -15.452 l 7.8444949,2.628449 5.656854,-2.65165 4.24264,3.005204 5.833631,-3.237437 3.712311,3.944543 z\" style=\"fill:url(#linearGradient3448);stroke:#888a85\"  />\n    <path d=\"m 30.383464,12.110514 c 4.108,0.159 7.304,-0.978 8.867,1.446 0.304,-3.9679998 -7.254,-8.8279998 -9.285,-8.4979998 0.813,0.498 0.418,7.0519998 0.418,7.0519998 z\" style=\"fill:url(#linearGradient3445);stroke:#868a84\" />\n    <path enable-background=\"new\" d=\"m 31.443464,11.086514 c 2.754,-0.019 4.106,-0.49 5.702,0.19 -1.299,-1.8809998 -4.358,-3.3439998 -5.728,-4.0279998 0.188,0.775 0.026,3.8379998 0.026,3.8379998 z\" style=\"opacity:0.36930003;fill:none;stroke:url(#linearGradient3442)\" />\n  </g>\n</g>\n</svg>";
//# sourceMappingURL=utils.d.ts.map