import { WidgetModel, DOMWidgetView, WidgetView } from './widget';
export declare function createErrorWidgetModel(error: unknown, msg?: string): typeof WidgetModel;
export declare class ErrorWidgetView extends DOMWidgetView {
    generateErrorMessage(): {
        msg?: string;
        stack: string;
    };
    render(): void;
}
export declare function createErrorWidgetView(error?: unknown, msg?: string): typeof WidgetView;
//# sourceMappingURL=errorwidget.d.ts.map