// Copyright (c) Jupyter Development Team.
// Distributed under the terms of the Modified BSD License.
export var shims;
(function (shims) {
    let services;
    (function (services) {
        /**
         * Public constructor
         * @param jsServicesKernel - @jupyterlab/services Kernel.IKernel instance
         */
        class CommManager {
            constructor(jsServicesKernel) {
                this.targets = Object.create(null);
                this.comms = Object.create(null);
                this.init_kernel(jsServicesKernel);
            }
            /**
             * Hookup kernel events.
             * @param  {Kernel.IKernel} jsServicesKernel - @jupyterlab/services Kernel.IKernel instance
             */
            init_kernel(jsServicesKernel) {
                this.kernel = jsServicesKernel; // These aren't really the same.
                this.jsServicesKernel = jsServicesKernel;
            }
            /**
             * Creates a new connected comm
             */
            async new_comm(target_name, data, callbacks, metadata, comm_id, buffers) {
                const c = this.jsServicesKernel.createComm(target_name, comm_id);
                const comm = new Comm(c);
                this.register_comm(comm);
                comm.open(data, callbacks, metadata, buffers);
                return comm;
            }
            /**
             * Register a comm target
             * @param  {string} target_name
             * @param  {(Comm, object) => void} f - callback that is called when the
             *                         comm is made.  Signature of f(comm, msg).
             */
            register_target(target_name, f) {
                const handle = this.jsServicesKernel.registerCommTarget(target_name, (jsServicesComm, msg) => {
                    // Create the comm.
                    const comm = new Comm(jsServicesComm);
                    this.register_comm(comm);
                    // Call the callback for the comm.
                    try {
                        return f(comm, msg);
                    }
                    catch (e) {
                        comm.close();
                        console.error(e);
                        console.error(new Error('Exception opening new comm'));
                    }
                });
                this.targets[target_name] = handle;
            }
            /**
             * Unregisters a comm target
             * @param  {string} target_name
             */
            unregister_target(target_name, f) {
                const handle = this.targets[target_name];
                handle.dispose();
                delete this.targets[target_name];
            }
            /**
             * Register a comm in the mapping
             */
            register_comm(comm) {
                this.comms[comm.comm_id] = Promise.resolve(comm);
                comm.kernel = this.kernel;
                return comm.comm_id;
            }
        }
        services.CommManager = CommManager;
        /**
         * Public constructor
         * @param  {IComm} jsServicesComm - @jupyterlab/services IComm instance
         */
        class Comm {
            constructor(jsServicesComm) {
                this.jsServicesComm = jsServicesComm;
            }
            /**
             * Comm id
             * @return {string}
             */
            get comm_id() {
                return this.jsServicesComm.commId;
            }
            /**
             * Target name
             * @return {string}
             */
            get target_name() {
                return this.jsServicesComm.targetName;
            }
            /**
             * Opens a sibling comm in the backend
             * @param  data
             * @param  callbacks
             * @param  metadata
             * @return msg id
             */
            open(data, callbacks, metadata, buffers) {
                const future = this.jsServicesComm.open(data, metadata, buffers);
                this._hookupCallbacks(future, callbacks);
                return future.msg.header.msg_id;
            }
            /**
             * Sends a message to the sibling comm in the backend
             * @param  data
             * @param  callbacks
             * @param  metadata
             * @param  buffers
             * @return message id
             */
            send(data, callbacks, metadata, buffers) {
                const future = this.jsServicesComm.send(data, metadata, buffers);
                this._hookupCallbacks(future, callbacks);
                return future.msg.header.msg_id;
            }
            /**
             * Closes the sibling comm in the backend
             * @param  data
             * @param  callbacks
             * @param  metadata
             * @return msg id
             */
            close(data, callbacks, metadata, buffers) {
                const future = this.jsServicesComm.close(data, metadata, buffers);
                this._hookupCallbacks(future, callbacks);
                return future.msg.header.msg_id;
            }
            /**
             * Register a message handler
             * @param  callback, which is given a message
             */
            on_msg(callback) {
                this.jsServicesComm.onMsg = callback.bind(this);
            }
            /**
             * Register a handler for when the comm is closed by the backend
             * @param  callback, which is given a message
             */
            on_close(callback) {
                this.jsServicesComm.onClose = callback.bind(this);
            }
            /**
             * Hooks callback object up with @jupyterlab/services IKernelFuture
             * @param  @jupyterlab/services IKernelFuture instance
             * @param  callbacks
             */
            _hookupCallbacks(future, callbacks) {
                if (callbacks) {
                    future.onReply = function (msg) {
                        if (callbacks.shell && callbacks.shell.reply) {
                            callbacks.shell.reply(msg);
                        }
                    };
                    future.onStdin = function (msg) {
                        if (callbacks.input) {
                            callbacks.input(msg);
                        }
                    };
                    future.onIOPub = function (msg) {
                        if (callbacks.iopub) {
                            if (callbacks.iopub.status && msg.header.msg_type === 'status') {
                                callbacks.iopub.status(msg);
                            }
                            else if (callbacks.iopub.clear_output &&
                                msg.header.msg_type === 'clear_output') {
                                callbacks.iopub.clear_output(msg);
                            }
                            else if (callbacks.iopub.output) {
                                switch (msg.header.msg_type) {
                                    case 'display_data':
                                    case 'execute_result':
                                    case 'stream':
                                    case 'error':
                                        callbacks.iopub.output(msg);
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    };
                }
            }
        }
        services.Comm = Comm;
    })(services = shims.services || (shims.services = {}));
})(shims || (shims = {}));
//# sourceMappingURL=services-shim.js.map