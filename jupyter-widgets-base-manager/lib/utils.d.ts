/**
 * Convert an ArrayBuffer to a hex string.
 */
export declare function bufferToHex(buffer: ArrayBuffer): string;
/**
 * Convert a hex string to an ArrayBuffer.
 */
export declare function hexToBuffer(hex: string): ArrayBuffer;
/**
 * Convert an ArrayBuffer to a base64 string.
 */
export declare function bufferToBase64(buffer: ArrayBuffer): string;
/**
 * Convert a base64 string to an ArrayBuffer.
 */
export declare function base64ToBuffer(base64: string): ArrayBuffer;
//# sourceMappingURL=utils.d.ts.map