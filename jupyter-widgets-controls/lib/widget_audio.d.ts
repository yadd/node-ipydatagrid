/// <reference types="backbone" />
import { DOMWidgetView } from '@jupyter-widgets/base';
import { CoreDOMWidgetModel } from './widget_core';
export declare class AudioModel extends CoreDOMWidgetModel {
    defaults(): Backbone.ObjectHash;
    static serializers: {
        value: {
            serialize: (value: any) => DataView;
        };
    };
}
export declare class AudioView extends DOMWidgetView {
    render(): void;
    update(): void;
    remove(): void;
    preinitialize(): void;
    el: HTMLAudioElement;
}
//# sourceMappingURL=widget_audio.d.ts.map